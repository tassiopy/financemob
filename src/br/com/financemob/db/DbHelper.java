package br.com.financemob.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class DbHelper extends SQLiteOpenHelper {

  private static final int DATABASE_VERSION = 10;
  private static final String DATABASE_NAME = "financemob";

  // Database creation sql statement
  private static final String DATABASE_CREATE_TB_USUARIO = "create table tb_usuario (" +
  													"id integer primary key autoincrement, " +
  													"login varchar(30) not null, " +
  													"email varchar(30), " +
  													"senha varchar(30), " +
  													"status_envio varchar(1) " +
  													")";
  
  private static final String DATABASE_CREATE_TB_CATEGORIA = "create table tb_categoria (" +
													"id integer primary key autoincrement, " +
													"descricao varchar(100) not null, " +
													"entradaSaida varchar(1) not null, " +
													"idUsuario integer not null, " +
													"status_envio varchar(1) " +
													")";						
  
  private static final String DATABASE_CREATE_TB_CARTEIRA = "create table tb_carteira (" +
													"id integer primary key autoincrement, " +
													"descricao varchar(100) not null, " +
													"agencia varchar(50), " +
													"numeroConta varchar(20), " +
													"saldo real not null, " +
													"idUsuario integer not null, " +
													"status_envio varchar(1) " +
													")";
  
  private static final String DATABASE_CREATE_TB_LANCAMENTO = "create table tb_lancamento (" +
													"id integer primary key autoincrement, " +
													"descricao varchar(50) not null, " +
													"dtVencimento varchar(20) not null, " +
													"idCategoria integer not null, " +
													"idCarteira integer not null, " +
													"valor real not null, " +
													"nrRepeticoes integer not null, " +
													"notificarVencimento varchar(1), " +
													"idUsuario integer not null " +
													")";


  public DbHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase database) {
    database.execSQL(DATABASE_CREATE_TB_USUARIO);
    database.execSQL(DATABASE_CREATE_TB_CATEGORIA);
    database.execSQL(DATABASE_CREATE_TB_CARTEIRA);
    database.execSQL(DATABASE_CREATE_TB_LANCAMENTO);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    Log.w(DbHelper.class.getName(),
        "Upgrading database from version " + oldVersion + " to "
            + newVersion + ", which will destroy all old data");
    db.execSQL("DROP TABLE IF EXISTS tb_usuario");
    db.execSQL("DROP TABLE IF EXISTS tb_categoria");
    db.execSQL("DROP TABLE IF EXISTS tb_carteira");
    db.execSQL("DROP TABLE IF EXISTS tb_lancamento");
    onCreate(db);
  }

} 