package br.com.financemob.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public abstract class AbstractDS<T> {
	
	private SQLiteDatabase database;
	private DbHelper dbHelper;
	private Context context;

	public AbstractDS(Context context) {
		dbHelper = new DbHelper(context);
		this.context = context;
	}
	
	public void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	protected SQLiteDatabase getDatabase() {
		if (database == null) {
			throw new IllegalAccessError("Datasource is not open!");
		}
		return database;
	}
	
	protected T create(ContentValues values) {
	    Long insertId = getDatabase().insert(getTableName(), null, values);
	    return find(insertId);
	}
	
	protected boolean update(ContentValues values, String where) {
		int updateId = getDatabase().update(getTableName(), values, where, null);
		return updateId > 0;
	}
	
	public T find(Long id) {
		return new QueryBuilder().select(getAllColumns())
				 .from(getTableName())
				 .where(getIdColumnName()+ " = "  + id)
				 .getSingleResultOrNull();
	}
	
	public T findBy(String where) {
		return new QueryBuilder().select(getAllColumns())
								 .from(getTableName())
		    					 .where(where)
								 .getSingleResultOrNull();
	}
	
	public List<T> getAll() {
	    return new QueryBuilder().select(getAllColumns())
								 .from(getTableName())
								 .getResultList();
	}
	
	public List<T> getListWhereFilter(String where) {
	    return new QueryBuilder().select(getAllColumns())
								 .from(getTableName())
								 .where(where)
								 .getResultList();
	}
	
	public void delete(Long id) {
	    getDatabase().delete(getTableName(), getIdColumnName()+ " = " + id, null);
	}

	protected String getIdColumnName() {
		return "id";
	}
	
	public Context getContext() {
		return context;
	}

	protected abstract T loadFromCursor(Cursor cursor);

	protected abstract String getTableName();
	protected abstract String[] getAllColumns();
	
	protected class QueryBuilder {
		
		private String[] columns;
		private String from;
		private String where;
		private String orderBy;
		private String groupBy;
		private String having;
		
		public QueryBuilder select(String[] columns) {
			this.columns = columns;
			return this;
		}
		
		public List<T> getResultList() {
			Cursor cursor = build();
			
			List<T> all = new ArrayList<T>();
		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
			      T object = loadFromCursor(cursor);
			      all.add(object);
			      cursor.moveToNext();
			}
		    cursor.close();
		    return all;
		}

		public QueryBuilder from(String from) {
			this.from = from;
			return this;
		}
		
		public QueryBuilder where(String where) {
			this.where = where;
			return this;
		}
		
		public QueryBuilder orderBy(String orderBy) {
			this.orderBy = orderBy;
			return this;
		}
		
		public QueryBuilder groupBy(String groupBy) {
			this.groupBy = groupBy;
			return this;
		}
		
		public QueryBuilder having(String having) {
			this.having = having;
			return this;
		}
		
		public Cursor build() {
			return getDatabase().query(from, columns, where, null, groupBy, having, orderBy);
		}
		
		public T getSingleResultOrNull() {
			Cursor cursor = build();
			cursor.moveToFirst();
			if (!cursor.isAfterLast()) {
			    T newObject = loadFromCursor(cursor);
			    cursor.close();
				return newObject;
			}
			return null;
		}
	}

}
