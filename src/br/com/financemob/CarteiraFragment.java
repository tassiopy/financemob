package br.com.financemob;

import java.util.List;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import br.com.financemob.adapter.CarteiraAdapter;
import br.com.financemob.controller.CadastroCarteiraActivity;
import br.com.financemob.model.Carteira;
import br.com.financemob.model.CarteiraDS;
import br.com.financemob.service.AuthenticationService;
import br.com.financemob.task.CarteiraTask;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CarteiraFragment extends Fragment {
	
	private ListView listCarteira;
	private CarteiraDS carteiraDS;
	private Context context;
	   
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.carteira_layout, container, false);
		
		listCarteira = (ListView) rootView.findViewById(R.id.listview_carteira);
	    context = container.getContext();
	    carteiraDS = new CarteiraDS(container.getContext());
		
		listCarteira.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long pos) {
				onListItemClick((Carteira) adapter.getItemAtPosition(position));
			}
		});
		
//		carregarCarteiraLista();
		carregarCarteiraListaWebService();
	    return rootView;
	}
	
	@SuppressWarnings("unchecked")
	private void carregarCarteiraListaWebService() {
		CarteiraTask carteiraTask = new CarteiraTask(context);
		List<Carteira> carteiras = (List<Carteira>) carteiraTask.executeTask(CarteiraTask.CONSULTAR_ACTION, AuthenticationService.instance(context).getUsuarioLogado());
		listCarteira.setAdapter(new CarteiraAdapter(context, R.layout.view_carteira, carteiras));		
	}

	public void carregarCarteiraLista() {
		carteiraDS.open();
		List<Carteira> carteiras = carteiraDS.getListWhereFilter("idUsuario = "+AuthenticationService.instance(context).getUsuarioLogado().getId());
		listCarteira.setAdapter(new CarteiraAdapter(context, R.layout.view_carteira, carteiras));
		carteiraDS.close();
	}
	
	private void onListItemClick(Carteira carteira){
		Intent intent = new Intent(context, CadastroCarteiraActivity.class);
		intent.putExtra("carteira", carteira);
		startActivity(intent);
	}

}
