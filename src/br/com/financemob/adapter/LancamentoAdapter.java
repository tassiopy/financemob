package br.com.financemob.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.financemob.R;
import br.com.financemob.model.Lancamento;

public class LancamentoAdapter extends ArrayAdapter<Lancamento>{

	private Context context;
	private int layoutResourceId;
	private List<Lancamento> lancamentos;
	
	public LancamentoAdapter(Context context, int resource, List<Lancamento> objects) {
		super(context, resource, objects);
		this.layoutResourceId = resource;
		this.context = context;
		this.lancamentos = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
	 
        Lancamento lancamento = lancamentos.get(position);
 
        TextView textDescricao = (TextView) convertView.findViewById(R.id.txt_lancamento_descricao);
        TextView textTipo = (TextView) convertView.findViewById(R.id.txt_lancamento_tipo);
        TextView textValor = (TextView) convertView.findViewById(R.id.txt_lancamento_valor);
        
        textDescricao.setText(lancamento.getDescricao());
//        textTipo.setText(lancamento.get);
        textValor.setText(lancamento.getValor().toString());
 
        return convertView;
	}
}
