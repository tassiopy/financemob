package br.com.financemob.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.financemob.R;
import br.com.financemob.model.Carteira;

public class CarteiraAdapter extends ArrayAdapter<Carteira> {
	
	private Context context;
	private int layoutResourceId;
	private List<Carteira> carteiras;
	
	public CarteiraAdapter(Context context, int resource, List<Carteira> objects) {
		super(context, resource, objects);
		this.layoutResourceId = resource;
		this.context = context;
		this.carteiras = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
	 
        Carteira carteira = carteiras.get(position);
 
        TextView textDescricao = (TextView) convertView.findViewById(R.id.txt_carteira_descricao);
        TextView textValor = (TextView) convertView.findViewById(R.id.txt_carteira_valor);
        
        textDescricao.setText(carteira.getDescricao());
        textValor.setText(carteira.getSaldo().toString());
 
        return convertView;
	}

}
