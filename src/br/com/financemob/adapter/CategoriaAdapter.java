package br.com.financemob.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.financemob.R;
import br.com.financemob.model.Categoria;

public class CategoriaAdapter extends ArrayAdapter<Categoria> {	

	private Context context;
	private int layoutResourceId;
	private List<Categoria> categorias;
	
	public CategoriaAdapter(Context context, int resource, List<Categoria> objects) {
		super(context, resource, objects);
		this.layoutResourceId = resource;
		this.context = context;
		this.categorias = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
	 
        Categoria categoria = categorias.get(position);
 
        TextView textDescricao = (TextView) convertView.findViewById(R.id.txt_categoria_descricao);
        TextView textTipo = (TextView) convertView.findViewById(R.id.txt_categoria_tipo);
        
        textDescricao.setText(categoria.getDescricao());
        textTipo.setText(categoria.getEntradaSaida().getLabel());
 
        return convertView;
	}

}
