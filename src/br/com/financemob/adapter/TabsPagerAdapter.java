package br.com.financemob.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import br.com.financemob.CarteiraFragment;
import br.com.financemob.CategoriaFragment;
import br.com.financemob.LancamentosFragment;

public class TabsPagerAdapter extends FragmentPagerAdapter	{
	
	public static final int TAB_LANCAMENTO = 0;
	public static final int TAB_CATEGORIA = 1;
	public static final int TAB_CARTEIRA = 2;
	
	private LancamentosFragment lancamentosFragment;
	private CategoriaFragment categoriaFragment;
	private CarteiraFragment carteiraFragment;

	public TabsPagerAdapter(FragmentManager fragmentManager){
		super(fragmentManager);
	}

	@Override
	public Fragment getItem(int index) {
		switch(index){
		case TAB_LANCAMENTO:
			return lancamentosFragment == null ? lancamentosFragment = new LancamentosFragment() : lancamentosFragment;
		case TAB_CATEGORIA:
			return categoriaFragment == null ? categoriaFragment = new CategoriaFragment() : categoriaFragment;
		case TAB_CARTEIRA:
			return carteiraFragment == null ? carteiraFragment = new CarteiraFragment() : carteiraFragment;
		}
		return null;
	}

	@Override
	public int getCount() {
		// numero de abas
		return 3;
	}
}
