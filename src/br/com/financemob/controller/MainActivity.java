package br.com.financemob.controller;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import br.com.financemob.CarteiraFragment;
import br.com.financemob.CategoriaFragment;
import br.com.financemob.LancamentosFragment;
import br.com.financemob.R;
import br.com.financemob.adapter.TabsPagerAdapter;
import br.com.financemob.service.AuthenticationService;

@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity implements ActionBar.TabListener{
	
	private static final int MENU_ITEM_CADASTRAR_LANCAMENTO = 0;
	private static final int MENU_ITEM_CADASTRAR_CATEGORIA = 1;
	private static final int MENU_ITEM_CADASTRAR_CARTEIRA = 2;
	private static final int MENU_ITEM_LOGOFF = 3;
	
	private ViewPager viewPage;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	
	private String[] tabs = {"Lan�amentos","Categorias","Carteiras"};	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        viewPage = (ViewPager) findViewById(R.id.pager);
        actionBar = getActionBar();
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        
        viewPage.setAdapter(mAdapter);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        for (String tab_name : tabs){
        	actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
        }
 
    }
    
    @Override
    protected void onResume() {
    	if (actionBar.getSelectedNavigationIndex()==TabsPagerAdapter.TAB_CATEGORIA){
    		CategoriaFragment fragment = (CategoriaFragment) mAdapter.getItem(TabsPagerAdapter.TAB_CATEGORIA);
    		fragment.carregarCategoriaLista();
    	} else if (actionBar.getSelectedNavigationIndex()==TabsPagerAdapter.TAB_CARTEIRA){
    		CarteiraFragment fragment = (CarteiraFragment) mAdapter.getItem(TabsPagerAdapter.TAB_CARTEIRA);
    		fragment.carregarCarteiraLista();
    	} else if (actionBar.getSelectedNavigationIndex()==TabsPagerAdapter.TAB_LANCAMENTO){
    		LancamentosFragment fragment = (LancamentosFragment) mAdapter.getItem(TabsPagerAdapter.TAB_LANCAMENTO);
    		fragment.carregarLancamentoLista();
    	}
    	super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
    	if (viewPage.getCurrentItem()==TabsPagerAdapter.TAB_CATEGORIA){
    		menu.getItem(MENU_ITEM_CADASTRAR_CATEGORIA).setVisible(true);
    		menu.getItem(MENU_ITEM_CADASTRAR_CARTEIRA).setVisible(false);
    		menu.getItem(MENU_ITEM_CADASTRAR_LANCAMENTO).setVisible(false);
    	} else if (viewPage.getCurrentItem()==TabsPagerAdapter.TAB_CARTEIRA){
    		menu.getItem(MENU_ITEM_CADASTRAR_CARTEIRA).setVisible(true);
    		menu.getItem(MENU_ITEM_CADASTRAR_CATEGORIA).setVisible(false);
    		menu.getItem(MENU_ITEM_CADASTRAR_LANCAMENTO).setVisible(false);
    	} else if (viewPage.getCurrentItem()==TabsPagerAdapter.TAB_LANCAMENTO){
    		menu.getItem(MENU_ITEM_CADASTRAR_LANCAMENTO).setVisible(true);
    		menu.getItem(MENU_ITEM_CADASTRAR_CATEGORIA).setVisible(false);
    		menu.getItem(MENU_ITEM_CADASTRAR_CARTEIRA).setVisible(false);
    	}
    	return super.onMenuOpened(featureId, menu);
    }
    
    @Override
    public void finish() {
    	openDialogOnFinish();
    }
    
    private void openDialogOnFinish() {
    	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    	alertDialogBuilder.setTitle("Financemob");
    	alertDialogBuilder.setMessage("Deseja realmente encerrar a sessão ?");
		alertDialogBuilder.setCancelable(false);
		alertDialogBuilder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				AuthenticationService.instance(MainActivity.this).logOff();
				startActivityLogin();
			}
		});
		alertDialogBuilder.setNegativeButton("Não",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
    
    private void startActivityCadastroCategoria() {
		Intent intent = new Intent(this, CadastroCategoriaActivity.class);
		startActivity(intent);
	}
    
    private void startActivityCadastroCarteira() {
    	Intent intent = new Intent(this, CadastroCarteiraActivity.class);
    	startActivity(intent);
	}
    
    private void startActivityCadastroLancamento() {
    	Intent intent = new Intent(this, CadastroLancamentoActivity.class);
    	startActivity(intent);
	}
    
    private void startActivityLogin(){
    	Intent intent = new Intent(this, LoginActivity.class);
    	startActivity(intent);
    	super.finish();
    }

	public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getOrder()) {
            case MENU_ITEM_LOGOFF:
                AuthenticationService.instance(this).logOff();
                startActivityLogin();
                return true;
            case MENU_ITEM_CADASTRAR_CATEGORIA:
            	startActivityCadastroCategoria();
            	return true;
            case MENU_ITEM_CADASTRAR_CARTEIRA:
            	startActivityCadastroCarteira();
            	return true;
            case MENU_ITEM_CADASTRAR_LANCAMENTO:
            	startActivityCadastroLancamento();
            	return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		viewPage.setCurrentItem(arg0.getPosition());
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}
    
}
