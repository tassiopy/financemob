package br.com.financemob.controller;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;
import br.com.financemob.R;
import br.com.financemob.model.Carteira;
import br.com.financemob.model.CarteiraDS;
import br.com.financemob.service.FinancemobException;
import br.com.financemob.task.CarteiraTask;

public class CadastroCarteiraActivity extends Activity implements OnClickListener{

	private EditText editDescricao;
	private EditText editAgencia;
	private EditText editNumConta;
	private EditText editSaldo;
	
	private CarteiraDS carteiraDS;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro_carteira);
		
		editDescricao = (EditText) findViewById(R.id.carteira_descricao);
		editAgencia = (EditText) findViewById(R.id.carteira_agencia);
		editNumConta = (EditText) findViewById(R.id.carteira_nconta);
		editSaldo  = (EditText) findViewById(R.id.carteira_saldo);
		
		carteiraDS = new CarteiraDS(this);
	}

	private void gravaCarteira() {
		String descricao = editDescricao.getText().toString().trim();
		String agencia = editAgencia.getText().toString().trim();
		String numeroConta = editNumConta.getText().toString().trim();
		String saldo = editSaldo.getText().toString().trim();
		
		try {
			carteiraDS.open();
			Carteira carteira = carteiraDS.createCarteira(descricao, agencia, numeroConta, saldo);
			enviarCadastroCarteira(carteira);
			Toast.makeText(this, getString(R.string.carteira_cadastro_sucesso), Toast.LENGTH_SHORT).show();
			finish();
		} catch (FinancemobException ex) {
			showEditTextError(ex.getMessage());
		} catch (Exception ex) {
			Toast.makeText(this, getString(R.string.carteira_cadastro_falha), Toast.LENGTH_SHORT).show();
			ex.printStackTrace();
		} finally {
			carteiraDS.close();
		}
	}
	
	private void enviarCadastroCarteira(Carteira carteira){
		CarteiraTask carteiraTask = new CarteiraTask(getApplicationContext());
		Carteira cart = (Carteira) carteiraTask.executeTask(CarteiraTask.CADASTRAR_ACTION, carteira);
		if (cart == null) {
			Toast.makeText(this, getString(R.string.usuario_envio_invalido), Toast.LENGTH_LONG).show();
			carteiraDS.updateStatusEnvio("P", carteira.getId());
		}
	}
	
	private void showEditTextError(String message) {
		if (message.equals(getString(R.string.carteira_descricao_invalida))){
			editDescricao.setError(message);
			editDescricao.requestFocus();
		} else if (message.equals(getString(R.string.carteira_saldo_invalido))){
			editSaldo.setError(message);
			editSaldo.requestFocus();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.cadastro_carteira, menu);
		return true;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btn_cadastrar_carteira:
			gravaCarteira();
			break;
		case R.id.btn_cancelar_carteria:
			finish();
			break;
		}	
	}
	
}
