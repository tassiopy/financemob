package br.com.financemob.controller;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import br.com.financemob.R;
import br.com.financemob.model.Carteira;
import br.com.financemob.model.CarteiraDS;
import br.com.financemob.model.Categoria;
import br.com.financemob.model.CategoriaDS;
import br.com.financemob.model.Lancamento;
import br.com.financemob.model.LancamentoDS;
import br.com.financemob.model.TrueFalseEnum;
import br.com.financemob.service.AuthenticationService;
import br.com.financemob.service.FinancemobException;

public class CadastroLancamentoActivity extends Activity implements OnClickListener{
	
	private EditText editDescricao;
	private EditText editVencimento;
	private Spinner spinnerCategoria;
	private Spinner spinnerCarteira;
	private EditText editValor;
	private EditText editRepeticoes;
	private CheckBox checkVencimento;
	
	private LancamentoDS lancamentoDS;
	private Lancamento lancamento;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro_lancamento);
		
		editDescricao = (EditText) findViewById(R.id.edit_lancamento_descricao);
		editVencimento = (EditText) findViewById(R.id.edit_lancamento_vencimento);
		spinnerCategoria = (Spinner) findViewById(R.id.spinner_lancamento_categoria);
		spinnerCarteira = (Spinner) findViewById(R.id.spinner_lancamento_carteira);
		editValor = (EditText) findViewById(R.id.edit_lancamento_valor);
		editRepeticoes = (EditText) findViewById(R.id.edit_numero_repeticoes);
		checkVencimento = (CheckBox) findViewById(R.id.check_notificar_vencimento);
		
		lancamentoDS = new LancamentoDS(this);
		
		ArrayAdapter<Categoria> adapterCategoria = new ArrayAdapter<Categoria>(this, android.R.layout.simple_spinner_dropdown_item, getCategoriaList());
		spinnerCategoria.setAdapter(adapterCategoria);
		spinnerCarteira.setAdapter(new ArrayAdapter<Carteira>(this, android.R.layout.simple_spinner_dropdown_item, getCarteiraList()));
		
		loadLancamentoFromBundle();
	}
	
	private void loadLancamentoFromBundle() {
		lancamento = (Lancamento) getIntent().getSerializableExtra("lancamento");
		if (lancamento != null){
			
		} else {
			lancamento = new Lancamento();
		}
	}
	
	private void gravarLancamento() {
		lancamento.setDescricao(editDescricao.getText().toString().trim());
		lancamento.setDtVencimento(editVencimento.getText().toString().trim());
		lancamento.setIdCategoria(((Categoria) spinnerCategoria.getSelectedItem()).getId());
		lancamento.setIdCarteira(((Carteira) spinnerCarteira.getSelectedItem()).getId());
		lancamento.setNotificarVencimento(checkVencimento.isChecked() ? TrueFalseEnum.S : TrueFalseEnum.N);
		String valor = editValor.getText().toString().trim();
		String nrRepeticoes = editRepeticoes.getText().toString().trim();		
		
		try {
			lancamentoDS.open();
			lancamento = lancamentoDS.createLancamento(lancamento, valor, nrRepeticoes);
			Toast.makeText(this, getString(R.string.lancamento_cadastro_sucesso), Toast.LENGTH_SHORT).show();
			finish();
		} catch (FinancemobException ex) {
			showEditTextError(ex.getMessage());
		} catch (Exception ex) {
			Toast.makeText(this, getString(R.string.lancamento_cadastro_falha), Toast.LENGTH_SHORT).show();
			ex.printStackTrace();
		} finally {
			lancamentoDS.close();
		}
	}
	
	private void showEditTextError(String message) {
		if (message.equals(getString(R.string.categoria_descricao_invalido))){
			editDescricao.setError(message);
			editDescricao.requestFocus();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.cadastro_lancamento, menu);
		return true;
	}
	
	private List<Categoria> getCategoriaList() {
		CategoriaDS categoriaDS = new CategoriaDS(this);
		categoriaDS.open();
		List<Categoria> lista = categoriaDS.getListWhereFilter("idUsuario = "+AuthenticationService.instance(this).getUsuarioLogado().getId());
		categoriaDS.close();
		return lista;
	}
	
	private List<Carteira> getCarteiraList(){
		CarteiraDS carteiraDS = new CarteiraDS(this);
		carteiraDS.open();
		List<Carteira> lista = carteiraDS.getListWhereFilter("idUsuario = "+AuthenticationService.instance(this).getUsuarioLogado().getId());
		carteiraDS.close();
		return lista;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btn_cadastrar:
			gravarLancamento();
			break;
		case R.id.btn_cancelar:
			finish();
			break;
		}		
	}

}
