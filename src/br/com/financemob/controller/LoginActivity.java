package br.com.financemob.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;
import br.com.financemob.R;
import br.com.financemob.model.Usuario;
import br.com.financemob.service.AuthenticationService;
import br.com.financemob.service.FinancemobException;
import br.com.financemob.service.LoginException;

public class LoginActivity extends Activity implements OnClickListener{
	
	private EditText editLogin;
	private EditText editSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_login);    
	    
	    editLogin = (EditText) findViewById(R.id.loginUsuario);
	    editSenha = (EditText) findViewById(R.id.senhaUsuario);
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	Usuario usuario = AuthenticationService.instance(this).getUsuarioLogado();
    	if (usuario != null) startMainActivity();
    }
    
    private void executarLogin() {
    	String login = editLogin.getText().toString().trim();
		String senha = editSenha.getText().toString().trim();
		try {
			validateLoginSenhaFields(login, senha);
			AuthenticationService.instance(this).login(login, senha);
			Toast.makeText(this, getString(R.string.login_sucesso), Toast.LENGTH_SHORT).show();
			startMainActivity();
		} catch (FinancemobException e){
			showEditTextError(e.getMessage());
		} catch (LoginException e) {
			Toast.makeText(this, "Erro ao logar: "+e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
    
    private void showEditTextError(String message) {
    	if (message.equals(getString(R.string.usuario_login_invalido))){
			editLogin.setError(message);
			editLogin.requestFocus();
		} else if (message.equals(getString(R.string.usuario_senha_invalida))){
			editSenha.setError(message);
			editSenha.requestFocus();
		}
	}

	private void validateLoginSenhaFields(String login, String senha) throws FinancemobException{
    	if (login.equals("")){
    		throw new FinancemobException(getString(R.string.usuario_login_invalido));
    	} else if (senha.equals("")){
    		throw new FinancemobException(getString(R.string.usuario_senha_invalida));
    	}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


	private void startMainActivity(){
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}

	public void startCadastroUsuarioActivity() {
		Intent i = new Intent(this, CadastroUsuarioActivity.class);
		startActivity(i);
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()){
		case R.id.btn_entrar:
			executarLogin(); 
			break;
		case R.id.textCadastro:
			startCadastroUsuarioActivity(); 
			break;
		}
	}
    
}
