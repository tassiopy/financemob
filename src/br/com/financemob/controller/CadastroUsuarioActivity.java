package br.com.financemob.controller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import br.com.financemob.R;
import br.com.financemob.model.Usuario;
import br.com.financemob.model.UsuarioDS;
import br.com.financemob.service.FinancemobException;
import br.com.financemob.service.TirarFotoService;
import br.com.financemob.task.UsuarioTask;

public class CadastroUsuarioActivity extends Activity implements OnClickListener{
	
	private EditText editLogin;
	private EditText editEmail;
	private EditText editSenha;
	private ImageView imgViewFoto;
	
	private UsuarioDS usuarioDS;
	private TirarFotoService tirarFotoService;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro_usuario);
		
		usuarioDS = new UsuarioDS(this);
		tirarFotoService = new TirarFotoService(this);

		editLogin = (EditText) findViewById(R.id.login_usuario);
		editEmail = (EditText) findViewById(R.id.email_usuario);
		editSenha = (EditText) findViewById(R.id.senha_usuario);
		imgViewFoto = (ImageView) findViewById(R.id.imgViewFoto);
	}
	
	private void gravarUsuario() {
		String login = editLogin.getText().toString();
		String email = editEmail.getText().toString();
		String senha = editSenha.getText().toString();
		
		try {
			usuarioDS.open();
			Usuario usuario = usuarioDS.createUsuario(login, email, senha);
			enviarCadastroUsuario(usuario);
			Toast.makeText(this, getString(R.string.usuario_cadastro_sucesso), Toast.LENGTH_LONG).show();
			finish();
		} catch (FinancemobException ex) {
			showEditTextError(ex.getMessage());
		} finally {
			usuarioDS.close();
		}
	}
	
	private void enviarCadastroUsuario(Usuario usuario){
		UsuarioTask usuarioTask = new UsuarioTask(getApplicationContext());
		Usuario user = usuarioTask.executeTask(UsuarioTask.SIGNUP_ACTION, usuario);
		if (user == null) {
			Toast.makeText(this, getString(R.string.usuario_envio_invalido), Toast.LENGTH_LONG).show();
			usuarioDS.updateStatusEnvio("P", usuario.getId());
		}
	}

	private void showEditTextError(String message) {
		if (message.equals(getString(R.string.usuario_nome_invalido))){
			editLogin.setError(message);
			editLogin.requestFocus();
		} else if (message.equals(getString(R.string.usuario_email_invalido))){
			editEmail.setError(message);
			editEmail.requestFocus();
		} else if (message.equals(getString(R.string.usuario_senha_invalida))){
			editSenha.setError(message);
			editSenha.requestFocus();
		}
	}
	
	public void tirarFoto() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tirarFotoService.getFotoUri());
        startActivityForResult(cameraIntent, TirarFotoService.TIRAR_FOTO);
    }
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TirarFotoService.TIRAR_FOTO && resultCode == RESULT_OK) {            
            Bitmap foto = tirarFotoService.scaleFoto();
            imgViewFoto.setImageBitmap(foto);
        }   
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cadastro_usuario, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.btn_cadastrar : 
				gravarUsuario(); break;
			case R.id.btn_cancelar : 
				finish(); break;
			case R.id.imgViewFoto :
				tirarFoto(); break;
		}
	}

}
