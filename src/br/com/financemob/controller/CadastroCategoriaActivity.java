package br.com.financemob.controller;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import br.com.financemob.R;
import br.com.financemob.model.Categoria;
import br.com.financemob.model.CategoriaDS;
import br.com.financemob.model.EntradaSaidaEnum;
import br.com.financemob.service.FinancemobException;

public class CadastroCategoriaActivity extends Activity implements OnClickListener{
	
	
	private EditText editDescricao;
	private RadioButton radioTipoEntrada;
	private RadioButton radioTipoSaida;
	
	private CategoriaDS categoriaDS;
	private Categoria categoria;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro_categoria);
		
		editDescricao = (EditText) findViewById(R.id.categoria_descricao);
		radioTipoEntrada = (RadioButton) findViewById(R.id.radio_tipo_entrada);
		radioTipoSaida = (RadioButton) findViewById(R.id.radio_tipo_saida);
		
		categoriaDS = new CategoriaDS(this);
		
		loadCategoriaFromBundle();
	}
	
	private void loadCategoriaFromBundle() {
		categoria = (Categoria) getIntent().getSerializableExtra("categoria");
		if (categoria != null){
			editDescricao.setText(categoria.getDescricao());
			if (categoria.getEntradaSaida()==EntradaSaidaEnum.E){
				radioTipoEntrada.setChecked(true);
			} else if (categoria.getEntradaSaida()==EntradaSaidaEnum.S){
				radioTipoSaida.setChecked(true);
			}
		}
	}

	private void gravarCategoria() {
		String descricao = editDescricao.getText().toString().trim();
		EntradaSaidaEnum entradaSaida = null;
		if (radioTipoEntrada.isChecked()) {
			entradaSaida = EntradaSaidaEnum.E;
		} else if (radioTipoSaida.isChecked()){
			entradaSaida = EntradaSaidaEnum.S;
		}
		
		try {
			categoriaDS.open();
			categoriaDS.createCategoria(descricao, entradaSaida);
			Toast.makeText(this, getString(R.string.categoria_cadastro_sucesso), Toast.LENGTH_SHORT).show();
			finish();
		} catch (FinancemobException ex) {
			showEditTextError(ex.getMessage());
		} catch (Exception ex) {
			Toast.makeText(this, getString(R.string.categoria_cadastro_falha), Toast.LENGTH_SHORT).show();
			ex.printStackTrace();
		} finally {
			categoriaDS.close();
		}
	}
	
	private void showEditTextError(String message) {
		if (message.equals(getString(R.string.categoria_descricao_invalido))){
			editDescricao.setError(message);
			editDescricao.requestFocus();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btn_cadastrar:
			gravarCategoria();
			break;
		case R.id.btn_cancelar:
			finish();
			break;
		}
	}

}
