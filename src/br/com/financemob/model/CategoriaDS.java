package br.com.financemob.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import br.com.financemob.R;
import br.com.financemob.db.AbstractDS;
import br.com.financemob.service.AuthenticationService;
import br.com.financemob.service.FinancemobException;

public class CategoriaDS extends AbstractDS<Categoria>{
	
	private static final String[] ALL_COLUMNS = new String[]{"id","descricao","entradaSaida","idUsuario"};
	
	public CategoriaDS(Context context) {
		super(context);
	}
	
	public Categoria createCategoria(String descricao, EntradaSaidaEnum entradaSaida) throws FinancemobException{
		validateCategoria(descricao, entradaSaida);
		ContentValues values = new ContentValues();
	    values.put("descricao", descricao);
	    values.put("entradaSaida", entradaSaida.name());
	    values.put("idUsuario", AuthenticationService.instance(getContext()).getUsuarioLogado().getId());
	    return create(values);
	}
	
	private void validateCategoria(String descricao, EntradaSaidaEnum entradaSaida) throws FinancemobException{
		if (descricao.equals("")){
			throw new FinancemobException(getContext().getString(R.string.categoria_descricao_invalido));
		} else if (entradaSaida == null){
			throw new FinancemobException(getContext().getString(R.string.categoria_tipo_invalido));
		}
	}

	@Override
	protected Categoria loadFromCursor(Cursor cursor) {
		Categoria categoria = new Categoria();
		categoria.setId(cursor.getLong(0));
		categoria.setDescricao(cursor.getString(1));
		categoria.setEntradaSaida(EntradaSaidaEnum.valueOf(cursor.getString(2)));
		categoria.setIdUsuario(cursor.getLong(3));
	    return categoria;
	}

	@Override
	protected String getTableName() {
		return Categoria.TABLE_NAME;
	}

	@Override
	protected String[] getAllColumns() {
		return ALL_COLUMNS;
	}

}
