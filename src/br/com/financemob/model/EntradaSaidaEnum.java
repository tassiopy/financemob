package br.com.financemob.model;

public enum EntradaSaidaEnum {
	
	E("Entrada"), S("Saída");
	
	private String label;
	
	private EntradaSaidaEnum(String label) {
		this.label = label;
	}
	
	public String getLabel(){
		return label;
	}

}
