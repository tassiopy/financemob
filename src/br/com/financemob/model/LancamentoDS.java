package br.com.financemob.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import br.com.financemob.db.AbstractDS;
import br.com.financemob.service.AuthenticationService;
import br.com.financemob.service.FinancemobException;

public class LancamentoDS extends AbstractDS<Lancamento>{
	
	private static final String[] ALL_COLUMNS = new String[]{"id", "descricao", "dtVencimento","idCategoria","idCarteira", "valor", "nrRepeticoes", "notificarVencimento", "idUsuario"};

	public LancamentoDS(Context context) {
		super(context);
	}
	
	public Lancamento createLancamento(Lancamento lancamento, String valor, String nrRepeticoes) throws FinancemobException{
		ContentValues contentValues = new ContentValues();		
		contentValues.put("descricao", lancamento.getDescricao());
		contentValues.put("dtVencimento", lancamento.getDtVencimento());
		contentValues.put("idCategoria", lancamento.getIdCategoria());
		contentValues.put("idCarteira", lancamento.getIdCarteira());
		contentValues.put("valor", Double.valueOf(valor));
		contentValues.put("nrRepeticoes", Integer.valueOf(nrRepeticoes));
		contentValues.put("notificarVencimento", lancamento.getNotificarVencimento().name());
		contentValues.put("idUsuario", AuthenticationService.instance(getContext()).getUsuarioLogado().getId());
		return create(contentValues);
	}

	@Override
	protected Lancamento loadFromCursor(Cursor cursor) {
		Lancamento lancamento = new Lancamento();
		lancamento.setId(cursor.getLong(0));
		lancamento.setDescricao(cursor.getString(1));
		lancamento.setDtVencimento(cursor.getString(2));
		lancamento.setIdCategoria(cursor.getLong(3));
		lancamento.setIdCarteira(cursor.getLong(4));
		lancamento.setValor(cursor.getDouble(5));
		lancamento.setNrRepeticoes(cursor.getInt(6));
		lancamento.setNotificarVencimento(TrueFalseEnum.valueOf(cursor.getString(7)));
		lancamento.setIdUsuario(cursor.getLong(8));
		return lancamento;
	}

	@Override
	protected String getTableName() {
		return Lancamento.TABLE_NAME;
	}

	@Override
	protected String[] getAllColumns() {
		return ALL_COLUMNS;
	}

}
