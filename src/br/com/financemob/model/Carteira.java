package br.com.financemob.model;

import java.io.Serializable;

public class Carteira implements Serializable{

	private static final long serialVersionUID = 8597974909810463061L;

	public static final String TABLE_NAME = "tb_carteira";
	
	private long id;
	private String descricao;
	private String agencia;
	private String numeroConta;
	private double saldo;
	private long idUsuario;
	private String statusEnvio;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getnConta() {
		return numeroConta;
	}
	public void setnConta(String nConta) {
		this.numeroConta = nConta;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}	
	public String getStatusEnvio() {
		return statusEnvio;
	}
	public void setStatusEnvio(String statusEnvio) {
		this.statusEnvio = statusEnvio;
	}
	@Override
	public String toString() {
		return descricao;
	}	
	
}
