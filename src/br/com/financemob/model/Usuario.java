package br.com.financemob.model;

public class Usuario {
	
	public static final String TABLE_NAME = "tb_usuario";
	
	private Long id;
	private String login;
	private String password;
	private String email;
	private String statusEnvio;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}	
	
	public String getStatusEnvio() {
		return statusEnvio;
	}
	
	public void setStatusEnvio(String statusEnvio) {
		this.statusEnvio = statusEnvio;
	}
	@Override
    public String toString() {
      return login;
    } 
}
