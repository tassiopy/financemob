package br.com.financemob.model;

import java.io.Serializable;


public class Lancamento implements Serializable{
	
	private static final long serialVersionUID = -6590412111684131583L;

	public static final String TABLE_NAME = "tb_lancamento";
	
	private Long id;
	private String descricao;
	private String dtVencimento;
	private Long idCategoria;
	private Long idCarteira;
	private Double valor;
	private Integer nrRepeticoes;
	private TrueFalseEnum notificarVencimento;
	private Long idUsuario;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDtVencimento() {
		return dtVencimento;
	}
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public Long getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(Long idCategoria) {
		this.idCategoria = idCategoria;
	}
	public Long getIdCarteira() {
		return idCarteira;
	}
	public void setIdCarteira(Long idCarteira) {
		this.idCarteira = idCarteira;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Integer getNrRepeticoes() {
		return nrRepeticoes;
	}
	public void setNrRepeticoes(Integer nrRepeticoes) {
		this.nrRepeticoes = nrRepeticoes;
	}
	public TrueFalseEnum getNotificarVencimento() {
		return notificarVencimento;
	}
	public void setNotificarVencimento(TrueFalseEnum notificarVencimento) {
		this.notificarVencimento = notificarVencimento;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}	
}
