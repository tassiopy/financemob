package br.com.financemob.model;

public enum TrueFalseEnum {
	
	S("Sim"), N("Não");
	
	private String label;
	
	private TrueFalseEnum(String label) {
		this.label = label;
	}
	
	public String getLabel(){
		return label;
	}

}
