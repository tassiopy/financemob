package br.com.financemob.model;

import java.io.Serializable;

public class Categoria implements Serializable{
	
	private static final long serialVersionUID = -489218632903837648L;
	public static final String TABLE_NAME = "tb_categoria";
	
	private Long id;
	private String descricao;
	private EntradaSaidaEnum entradaSaida;
	private Long idUsuario;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
	public EntradaSaidaEnum getEntradaSaida() {
		return entradaSaida;
	}
	public void setEntradaSaida(EntradaSaidaEnum entradaSaida) {
		this.entradaSaida = entradaSaida;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}
