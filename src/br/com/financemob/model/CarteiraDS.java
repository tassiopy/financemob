package br.com.financemob.model;

import br.com.financemob.R;
import br.com.financemob.db.AbstractDS;
import br.com.financemob.service.AuthenticationService;
import br.com.financemob.service.FinancemobException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class CarteiraDS extends AbstractDS<Carteira>{
	
	private static final String[] ALL_COLUMNS = new String[] {"id","descricao","agencia","numeroConta","saldo","idUsuario","status_envio"};
	
	public CarteiraDS(Context context) {
		super(context);
	}

	public Carteira createCarteira(String descricao, String agencia, String numeroConta, String saldo) throws FinancemobException{
		validateCarteira(descricao, saldo);
		ContentValues values = new ContentValues();
		values.put("descricao", descricao);
		values.put("agencia", agencia);
		values.put("numeroConta", numeroConta);
		values.put("saldo", Double.parseDouble(saldo));
		values.put("idUsuario", AuthenticationService.instance(getContext()).getUsuarioLogado().getId());
		values.put("status_envio", "S");
		return create(values);
	}
	
	public void updateStatusEnvio(String status, Long id){
		ContentValues contentValues = new ContentValues();
		contentValues.put("status_envio", status);
		System.out.println(update(contentValues, "id = " + id));
	}
	
	private void validateCarteira(String descricao, String saldo) throws FinancemobException{
		if (descricao.equals("")){
			throw new FinancemobException(getContext().getString(R.string.carteira_descricao_invalida));
		} else if (saldo.equals("")){
			throw new FinancemobException(getContext().getString(R.string.carteira_saldo_invalido));
		}
	}
	
	@Override
	protected Carteira loadFromCursor(Cursor cursor) {
		Carteira carteira = new Carteira();
		carteira.setId(cursor.getLong(0));
		carteira.setDescricao(cursor.getString(1));
		carteira.setAgencia(cursor.getString(2));
		carteira.setnConta(cursor.getString(3));
		carteira.setSaldo(cursor.getDouble(4));
		carteira.setIdUsuario(cursor.getLong(5));
		carteira.setStatusEnvio(cursor.getString(6));
		return carteira;
	}

	@Override
	protected String getTableName() {
		return Carteira.TABLE_NAME;
	}

	@Override
	protected String[] getAllColumns() {
		return ALL_COLUMNS;
	}
}
