package br.com.financemob.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import br.com.financemob.R;
import br.com.financemob.db.AbstractDS;
import br.com.financemob.service.FinancemobException;

public class UsuarioDS extends AbstractDS<Usuario> {

	private static final String[] ALL_COLUMNS = new String[]{"id","login","email","senha", "status_envio"};
	
	public UsuarioDS(Context context) {
		super(context);
	}

	public Usuario createUsuario(String login, String email, String senha) throws FinancemobException{
		validateUsuario(login, email, senha);
		ContentValues values = new ContentValues();
	    values.put("login", login);
	    values.put("email", email);
	    values.put("senha", senha);
	    values.put("status_envio", "S");
	    return create(values);
	}
	
	public void updateStatusEnvio(String status, Long id){
		ContentValues contentValues = new ContentValues();
		contentValues.put("status_envio", status);
		System.out.println(update(contentValues, "id = " + id));
	}
	
	private void validateUsuario(String login, String email, String senha) throws FinancemobException{
		if (login==null || login.trim().equals("")){
			throw new FinancemobException(getContext().getString(R.string.usuario_nome_invalido));
		} else if (email==null || email.trim().equals("")){
			throw new FinancemobException(getContext().getString(R.string.usuario_email_invalido));
		} else if (senha==null || senha.trim().equals("")) {
			throw new FinancemobException(getContext().getString(R.string.usuario_senha_invalida));
		}
	}

	@Override
	protected Usuario loadFromCursor(Cursor cursor) {
		Usuario usuario = new Usuario();
	    usuario.setId(cursor.getLong(0));
	    usuario.setLogin(cursor.getString(1));
	    usuario.setEmail(cursor.getString(2));
	    usuario.setPassword(cursor.getString(3));
	    usuario.setStatusEnvio(cursor.getString(4));
	    return usuario;
	}

	@Override
	protected String getTableName() {
		return Usuario.TABLE_NAME;
	}

	@Override
	protected String[] getAllColumns() {
		return ALL_COLUMNS;
	}
}
