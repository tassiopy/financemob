package br.com.financemob.task;

import java.util.concurrent.ExecutionException;

import android.os.AsyncTask;
import android.util.Log;

public abstract class Task<E, P> extends AsyncTask<E, Void, P>{
	
	public abstract P executeInBackground(E... params);
	
	public P executeTask(E... params){
		try {
			return execute(params).get();
		} catch (InterruptedException e) {			
			Log.e(getClass().getSimpleName(), e.getMessage());
		} catch (ExecutionException e) {		
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		return null;
	}

	@Override
	protected P doInBackground(E... params) {		
		return executeInBackground(params);
	}

}
