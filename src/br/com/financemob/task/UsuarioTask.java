package br.com.financemob.task;

import android.app.ProgressDialog;
import android.content.Context;
import br.com.financemob.model.Usuario;
import br.com.financemob.service.UsuarioWebService;

public class UsuarioTask extends Task<Object, Usuario>{
	
	public static final String SIGNUP_ACTION = "signup";
	
	private ProgressDialog progressDialog;
	private Context context;
	
	public UsuarioTask(Context context) {
		this.context = context;
	}

	@Override
	public Usuario executeInBackground(Object... params) {
		String action = (String) params[0];
		UsuarioWebService webService = new UsuarioWebService();
		if (SIGNUP_ACTION.equals(action)){
			return webService.signUp((Usuario) params[1]);
		}
		return null;
	}
	
	@Override
	protected void onPreExecute() {
//		progressDialog = ProgressDialog.show(context, "Enviando...", "Enviando usuario!");
	}
	
	@Override
	protected void onPostExecute(Usuario result) {
//		progressDialog.dismiss();
	}

}
