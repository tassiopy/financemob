package br.com.financemob.task;

import android.app.ProgressDialog;
import android.content.Context;
import br.com.financemob.model.Usuario;
import br.com.financemob.service.UsuarioWebService;

public class LoginTask extends Task<String, Usuario>{
	
	private ProgressDialog progressDialog;
	private Context context;
	
	public LoginTask(Context context) {
		this.context = context;
	}
	
	public Usuario executeLogIn(String login, String senha){
		return executeTask(login, senha);
	}
	
	@Override
	public Usuario executeInBackground(String... params) {
		UsuarioWebService webService = new UsuarioWebService();
		return webService.login(params[0], params[1]);
	}
	
	@Override
	protected void onPreExecute() {
		progressDialog = new ProgressDialog(context);
		progressDialog.show();
	}
	
	@Override
	protected void onPostExecute(Usuario result) {
		progressDialog.dismiss();
	}

}
