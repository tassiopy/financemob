package br.com.financemob.task;

import android.content.Context;
import br.com.financemob.model.Carteira;
import br.com.financemob.model.Usuario;
import br.com.financemob.service.CarteiraWebService;

public class CarteiraTask extends Task<Object, Object>{
	
	public static final String CADASTRAR_ACTION = "cadastrar";
	public static final String CONSULTAR_ACTION = "consultar";
	
	private Context context;
	
	public CarteiraTask(Context context) {
		this.context = context;
	}

	@Override
	public Object executeInBackground(Object... params) {
		CarteiraWebService service = new CarteiraWebService();
		String action = (String) params[0];
		if (CADASTRAR_ACTION.equals(action)) {
			return service.cadastrarCarteira((Carteira) params[1]);
		} else if (CONSULTAR_ACTION.equals(action)) {
			return service.getCarteiraListByUsuario((Usuario) params[1]);
		}
		return null;
	}

}
