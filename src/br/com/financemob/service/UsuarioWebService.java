package br.com.financemob.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;
import br.com.financemob.model.Usuario;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UsuarioWebService {
	
	private static final String URL_LOGIN = "http://pythonws-avantirise.rhcloud.com/auth/login";
	private static final String URL_SIGNUP = "http://pythonws-avantirise.rhcloud.com/auth/signup";
	
	private HttpClient httpClient = new DefaultHttpClient();
	private Gson gson = new Gson();
	
	public Usuario login(String login, String senha) {
		try {
			HttpPost post =  new HttpPost(URL_LOGIN);
			post.setHeader("User-Agent", "user-agent");
			post.addHeader("Content-Type", "application/json");	
			String strParam = String.format("{\"login\": \"%s\", \"password\":\"%s\"}", login, senha);
		
			StringEntity params = new StringEntity(strParam);
			post.setEntity(params);
			HttpResponse response = httpClient.execute(post);			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line);
			}
			if (response.getStatusLine().getStatusCode() != 200) return null;
			Map<String, Object> map = gson.fromJson(sb.toString(), new TypeToken<HashMap<String, Object>>(){}.getType());
			String json = String.valueOf(map.get("user"));		
			return gson.fromJson(json, Usuario.class);
		} catch (ClientProtocolException e) {		
			Log.e(getClass().getSimpleName(), e.getMessage());
		} catch (IOException e) {			
			Log.e(getClass().getSimpleName(), e.getMessage());
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		return null;		
	}
	
	public Usuario signUp(Usuario usuario){
		try {
			HttpPost post =  new HttpPost(URL_SIGNUP);
			post.setHeader("User-Agent", "user-agent");
			post.addHeader("Content-Type", "application/json");
		
			String json = gson.toJson(usuario);
			StringEntity params = new StringEntity(json);
			post.setEntity(params);
			HttpResponse response = httpClient.execute(post);
			int statusCode = response.getStatusLine().getStatusCode();			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line);
			}			
			return statusCode == 200 ? usuario : null;
		} catch (ClientProtocolException e) {		
			Log.e(getClass().getSimpleName(), e.getMessage());
		} catch (IOException e) {			
			Log.e(getClass().getSimpleName(), e.getMessage());
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		return null;
	}

}
