package br.com.financemob.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

public class TirarFotoService {
	
	public static final int TIRAR_FOTO = 0;
	private Context context;
	private Uri fotoUri;
	
	public TirarFotoService(Context context){
		this.context = context;
		tirarFoto();
	}
	
	public void tirarFoto() {     
        File f = null;
        try {
            f = createImageFile();
        } catch (java.io.IOException e) {
            Toast.makeText(context, "Erro na criação do arquivo", Toast.LENGTH_SHORT).show();
        }
        fotoUri = Uri.fromFile(f);      
    }
	
	public Bitmap scaleFoto() {
        BitmapFactory.Options bmo = new BitmapFactory.Options();
        bmo.inPreferredConfig = Config.ARGB_8888;
        Bitmap foto = BitmapFactory.decodeFile(fotoUri.getPath(), bmo);
        foto = Bitmap.createScaledBitmap(foto, 100, 100, true);
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(fotoUri.getPath());
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
        case ExifInterface.ORIENTATION_ROTATE_90:
            foto = rotateImage(90, foto);
            break;
        case ExifInterface.ORIENTATION_ROTATE_180:
            foto = rotateImage(180, foto);
            break;
        }
        return foto;
	}
	 
	 public static Bitmap rotateImage(final int degree, Bitmap photo) {
		Matrix matrix = new Matrix();
		matrix.setRotate(degree);
		return Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), matrix, true);
	}
	    
    @SuppressLint("SimpleDateFormat")
	private File createImageFile() throws java.io.IOException {        
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "img_" + timeStamp;       
        File image = File.createTempFile(imageFileName, ".temp", getAlbumDir());
        return image;
    }
    
    private File getAlbumDir() {
        File result = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toURI());        
        if (!result.exists()) result.mkdir();        
        return result;
    }

	public Uri getFotoUri() {
		return fotoUri;
	} 

}
