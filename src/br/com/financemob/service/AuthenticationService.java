package br.com.financemob.service;

import android.content.Context;
import android.content.SharedPreferences;
import br.com.financemob.R;
import br.com.financemob.model.Usuario;
import br.com.financemob.model.UsuarioDS;
import br.com.financemob.task.LoginTask;

public class AuthenticationService {
	
	private Context context;
	private SharedPreferences sharedPreferences;
	private static Usuario usuario;
	private static AuthenticationService instance;
	private static final String USUARIO_LOGADO_KEY = "idUsuarioLogado";
	
	public static AuthenticationService instance(Context context){ 
		if (instance==null){
			instance = new AuthenticationService(context);
		}
		return instance;
	}
	
	private AuthenticationService(Context context) {
		this.context = context;
		this.sharedPreferences = context.getSharedPreferences("br.com.financemob", Context.MODE_PRIVATE);
	}
	
	public SharedPreferences getSharedPreferences(){
		return sharedPreferences;
	}
	
	public Usuario getUsuarioLogado(){
		if (usuario == null){
			Long idUsuario = sharedPreferences.getLong(USUARIO_LOGADO_KEY, 0L);
			if (idUsuario != 0L){
				UsuarioDS usuarioDS = new UsuarioDS(context);
				usuarioDS.open();
				setUsuarioLogado(usuarioDS.find(idUsuario));
				usuarioDS.close();
			}
		}
		return usuario;
	}
	
	private void setUsuarioLogado(Usuario usuario) {
		Long idUsuario = usuario == null ? 0L : usuario.getId();		
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putLong(USUARIO_LOGADO_KEY, idUsuario);
		editor.commit();		
		AuthenticationService.usuario = usuario;
	}
	
	public void login(String login, String senha) throws LoginException {
		
		LoginTask loginTask = new LoginTask(context);
		
		setUsuarioLogado(loginTask.executeLogIn(login, senha));
		
		if (usuario == null) { 
			throw new LoginException(context.getString(R.string.login_senha_invalido));
		}
	}
	
	public void logOff(){
		setUsuarioLogado(null);
	}

}
