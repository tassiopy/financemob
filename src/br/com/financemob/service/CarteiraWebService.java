package br.com.financemob.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;
import br.com.financemob.model.Carteira;
import br.com.financemob.model.Usuario;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class CarteiraWebService {
	
	private static final String URL_CADASTRO = "http://pythonws-avantirise.rhcloud.com/finance/carteiras";
	private static final String URL_CONSULTAR = "http://pythonws-avantirise.rhcloud.com/finance/carteiras/%d";
	
	private HttpClient httpClient = new DefaultHttpClient();
	private Gson gson = new Gson();
	
	public Carteira cadastrarCarteira(Carteira carteira){
		try {
			HttpPost post =  new HttpPost(URL_CADASTRO);
			post.setHeader("User-Agent", "user-agent");
			post.addHeader("Content-Type", "application/json");	
			String strParam = gson.toJson(carteira);
		
			StringEntity params = new StringEntity(strParam);
			post.setEntity(params);
			HttpResponse response = httpClient.execute(post);	
			int statusCode = response.getStatusLine().getStatusCode();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line);
			}
			return statusCode == 200 ? carteira : null;
		} catch (ClientProtocolException e) {		
			Log.e(getClass().getSimpleName(), e.getMessage());
		} catch (IOException e) {			
			Log.e(getClass().getSimpleName(), e.getMessage());
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		return null;		
	}
	
	public List<Carteira> getCarteiraListByUsuario(Usuario usuario){
		try {
			HttpGet request = new HttpGet(String.format(URL_CONSULTAR, usuario.getId()));
			
			HttpResponse response = httpClient.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = bufReader.readLine()) != null) {
				sb.append(line);
			}
			if (response.getStatusLine().getStatusCode() != 200) return null;
			System.out.println(sb.toString());
			Map<String, Object> map = gson.fromJson(sb.toString(), new TypeToken<HashMap<String, Object>>(){}.getType());
			String json = String.valueOf(map.get("results"));
			System.out.println(json);
			List<Carteira> carteiras = gson.fromJson(json, new TypeToken<ArrayList<Carteira>>(){}.getType());			
			return carteiras;
		} catch (Exception e){
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		return null;
	}

}
