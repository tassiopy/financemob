package br.com.financemob.service;

public class FinancemobException extends Exception {

	private static final long serialVersionUID = -7696753405576703956L;
	
	public FinancemobException(String mensagem) {
		super(mensagem);
	}

}
