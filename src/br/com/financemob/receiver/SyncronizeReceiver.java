package br.com.financemob.receiver;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import br.com.financemob.model.Carteira;
import br.com.financemob.model.CarteiraDS;
import br.com.financemob.model.Usuario;
import br.com.financemob.model.UsuarioDS;
import br.com.financemob.task.CarteiraTask;
import br.com.financemob.task.UsuarioTask;

public class SyncronizeReceiver extends BroadcastReceiver {

	@SuppressWarnings("deprecation")
	@Override
	public void onReceive(Context context, Intent intent) {
     
        NetworkInfo currentNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);        

        if(currentNetworkInfo != null && currentNetworkInfo.isConnected()){
        	enviarUsuariosPendentes(context); 
        	enviarCarteirasPendentes(context);
        }
	}
	
	private void enviarUsuariosPendentes(Context context){
		UsuarioDS usuarioDS = new UsuarioDS(context);
		UsuarioTask task = new UsuarioTask(context);
		usuarioDS.open();		
		List<Usuario> usuariosPendentes = usuarioDS.getListWhereFilter("status_envio = 'P'");
		for (Usuario usuarioPendente : usuariosPendentes){
			if (task.executeTask(UsuarioTask.SIGNUP_ACTION, usuarioPendente) != null) {
				usuarioDS.updateStatusEnvio("S", usuarioPendente.getId());
			}			
		}
		usuarioDS.close();
	}
	
	private void enviarCarteirasPendentes(Context context){
		CarteiraDS carteiraDS = new CarteiraDS(context);
		CarteiraTask task = new CarteiraTask(context);
		carteiraDS.open();		
		List<Carteira> carteirasPendentes = carteiraDS.getListWhereFilter("status_envio = 'P'");
		for (Carteira carteiraPendente : carteirasPendentes){
			if (task.executeTask(CarteiraTask.CADASTRAR_ACTION, carteiraPendente) != null) {
				carteiraDS.updateStatusEnvio("S", carteiraPendente.getId());
			}			
		}
		carteiraDS.close();
	}	
	

}
