package br.com.financemob;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import br.com.financemob.adapter.LancamentoAdapter;
import br.com.financemob.model.Lancamento;
import br.com.financemob.model.LancamentoDS;
import br.com.financemob.service.AuthenticationService;

@SuppressLint("NewApi")
public class LancamentosFragment extends Fragment {
	
	private ListView listLancamento;
	private LancamentoDS lancamentoDS;
	private Context context;
	   
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.lancamento_layout, container, false);
		
		listLancamento = (ListView) rootView.findViewById(R.id.listview_lancamento);
	    this.context = container.getContext();
	    lancamentoDS = new LancamentoDS(container.getContext());
		
	    carregarLancamentoLista();
	    return rootView;
	}
	
	public void carregarLancamentoLista() {
		if(lancamentoDS==null) return;
		lancamentoDS.open();
		List<Lancamento> lancamentos = lancamentoDS.getListWhereFilter("idUsuario = "+AuthenticationService.instance(context).getUsuarioLogado().getId());
		listLancamento.setAdapter(new LancamentoAdapter(context, R.layout.view_lancamento, lancamentos));
		lancamentoDS.close();
	}
}
