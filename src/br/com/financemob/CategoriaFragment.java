package br.com.financemob;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import br.com.financemob.adapter.CategoriaAdapter;
import br.com.financemob.controller.CadastroCategoriaActivity;
import br.com.financemob.model.Categoria;
import br.com.financemob.model.CategoriaDS;
import br.com.financemob.service.AuthenticationService;

public class CategoriaFragment extends Fragment {
	
	private ListView listCategoria;
	private CategoriaDS categoriaDS;
	private Context context;
	   
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.categoria_layout, container, false);
		
		listCategoria = (ListView) rootView.findViewById(R.id.listview_categoria);
	    this.context = container.getContext();
		categoriaDS = new CategoriaDS(container.getContext());
		
		listCategoria.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long pos) {
				onListItemClick((Categoria) adapter.getItemAtPosition(position));
			}
		});
		
	    carregarCategoriaLista();
	    return rootView;
	}
	
	public void carregarCategoriaLista() {
		categoriaDS.open();
		List<Categoria> categorias = categoriaDS.getListWhereFilter("idUsuario = "+AuthenticationService.instance(context).getUsuarioLogado().getId());
		listCategoria.setAdapter(new CategoriaAdapter(context, R.layout.view_categoria, categorias));
		categoriaDS.close();
	}
	
	private void onListItemClick(Categoria categoria){
		Intent intent = new Intent(context, CadastroCategoriaActivity.class);
		intent.putExtra("categoria", categoria);
		startActivity(intent);
	}

}
